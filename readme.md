#krumpy

##Arguments

| Argument    | Description |
| :-----------|:------------|
| name        | Sets the project/theme name across multiple files. Removes all special chars (including space and dashes)

##Options
See "Initialize Files" section below for full list of files affected by these options.

| Option      | Description |
| :-----------|:------------|
| author      | Sets the name of the author in "Initialize Files". 
| author_email| Sets the author's email in "Initialize Files". 
| theme_version| Sets the project/theme version number "Initialize Files" 
| initialize | Changes to base/ and then runs `composer install`, `npm install`, `gulp build`, and `docker-compose up -d`

##Initialize Files
When "Initialize Files" are referenced in this readme, they refer to the following files. Not all files are impacted by 
each argument/option passed to krumpy. All files are relative to the base/ directory:
* composer.json
* gulpfile.js
* package.json
* docker-compose.yml
* src/assets/css/modules/_theme-comments.scss

##Examples

####Create and Initialize Website 
`./krumpy create:website "Website Name" --author="Jeremy Duenas" --author_email="jeremy@icongraphics.com" --theme_verions="1.3.2" --initialize=1`  
Also overrides all options.

##changelog