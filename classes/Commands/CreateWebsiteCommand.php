<?php

namespace krumpy\Commands;

use krumpy\Creators\Website;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateWebsiteCommand extends Command
{
    public function configure()
    {
        $this->setName('create:website')
             ->setDescription('Create a WordPree theme')
             ->addArgument('name', InputArgument::REQUIRED, 'The theme\'s name');

        $this->addOption(
            'author',
            null,
            InputArgument::OPTIONAL,
            'The theme\s author',
            'Icon Graphics'
        );

        $this->addOption(
            'author_email',
            null,
            InputArgument::OPTIONAL,
            'The theme\'s author\'s email',
            'jeremy@icongraphics.com'
        );

        $this->addOption(
            'theme_version',
            null,
            InputArgument::OPTIONAL,
            'The theme\'s version',
            '1.0.0'
        );

        $this->addOption(
            'initialize',
            null,
            InputArgument::OPTIONAL,
            'Should krumpy run composer, npm, gulp, etc.?',
            "false"
        );

    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $website = new Website(
            $input->getArgument('name'),
            $input->getOption('author'),
            $input->getOption('author_email'),
            $input->getOption('version')
        );

        if($input->getOption('initialize') === "true" || $input->getOption('initialize') === "1")
        {
            $website->initialize();
        }
    }
}
