<?php

namespace krumpy\Creators;


use krumpy\Helpers\SanitizeString;

class Website
{
    public  $theme_name,
            $theme_author,
            $theme_author_email = "jeremy@icongraphics.com",
            $theme_version,
            $db_prefix,
            $placeholders;
    
    public function __construct(string $theme_name, string $theme_author, string $theme_author_email, string $theme_version)
    {
        $this->theme_name = $this->getThemeName($theme_name);
        $this->theme_author = $theme_author;
        $this->theme_author_email = $this->getThemeAuthorEmail($theme_author_email);
        $this->theme_version = $theme_version;
        $this->db_prefix = $this->getDbPrefix();
        $this->placeholders = $this->setPlaceholders();

        $this->replacePlaceholders();
    }

    /**
     * @return array
     * Where all "search and replace" placeholders are defined.
     */
    protected function setPlaceholders()
    {
        return [
            '<PLACEHOLDER__DBPREFIX>' => $this->db_prefix,
            '<PLACEHOLDER__THEMENAME>' => $this->theme_name,
            '<PLACEHOLDER__THEMEAUTHOR>' => $this->theme_author,
            '<PLACEHOLDER__THEMEVERSION>' => $this->theme_version,
            '<PLACEHOLDER__THEMEAUTHOREMAIL>' => $this->theme_author_email
        ];
    }

    /**
     * @param string $theme_name
     * @return SanitizeString
     * Cleans up user input for the theme name, removes special characters.
     */
    protected function getThemeName(string $theme_name)
    {
        $sanitized_theme_name = new SanitizeString($theme_name);

        return $sanitized_theme_name->removeDashes()
                                    ->removeSpecialCharacters()
                                    ->makeLowerCase();
    }

    /**
     * @param $email
     * @return mixed|string
     * Verifies that the provided author email is a actually an email address, if not, returns the default as defined
     * in the Website class.
     */
    protected function getThemeAuthorEmail($email)
    {
        if(filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            return $email;
        } else{
            return $this->theme_author_email;
        }
    }

    /**
     * @return string
     * Generates a random alphanumeric prefix for the databases
     */
    protected function getDbPrefix()
    {
        return  bin2hex(openssl_random_pseudo_bytes(2)) . "_";
    }


    /**
     * Iterates through all of the Initialize Files and performs a search/replace based off of the predefined
     * placeholders.
     */
    protected function replacePlaceholders()
    {
        $base_dir = __DIR__ . '/../../base';
        $files = [
            $base_dir . '/composer.json',
            $base_dir . '/gulpfile.js',
            $base_dir . '/package.json',
            $base_dir . '/docker-compose.yml',
            $base_dir . '/src/assets/css/modules/_theme-comments.scss'
        ];

        foreach($files as $file)
        {
            $file_contents = file_get_contents($file);
            $file_contents = $this->searchAndReplace($file_contents);
            file_put_contents($file, $file_contents);
        }

    }

    /**
     * @param string $file_contents
     * @return mixed|string
     * Searches through provided string for values in the placeholder array and replaces them as needed.
     */
    protected function searchAndReplace(string $file_contents)
    {
        foreach($this->placeholders as $search => $replace)
        {
            $file_contents = str_replace($search, $replace, $file_contents);
        }

        return $file_contents;
    }

    /**
     * Gets resulting files ready for use.
     *
     * Not really sure if this is a good idea, tbh. I doubt that I'd ever really keep krumpy files in the base dir
     * of a WP theme project, but maybe? Automation for automation's sake is totally cool, right?
     */
    public function initialize()
    {
        chdir('base');
        exec('composer install', $terminal_output);
        exec('npm install', $terminal_output);
        exec('gulp build', $terminal_output);
        exec('docker-compose up -d', $terminal_output);

        foreach($terminal_output as $output){
            echo $output . "\n";
        }
    }
}