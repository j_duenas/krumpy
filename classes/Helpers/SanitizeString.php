<?php

namespace krumpy\Helpers;


class SanitizeString
{
    protected $string;

    public function __construct(string $string)
    {
        $this->string = $string;
    }

    public function __toString(){
        return $this->string;
    }

    public function removeDashes()
    {
        $this->string = str_replace(['-', '_'], '', $this->string);

        return $this;
    }

    public function removeSpecialCharacters()
    {
        $this->string = preg_replace('/[^A-Za-z0-9\-]/', '', $this->string);

        return $this;
    }

    public function makeLowerCase()
    {
        $this->string = strtolower($this->string);

        return $this;
    }
}