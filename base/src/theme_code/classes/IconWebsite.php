<?php
namespace icong;
use Timber\Menu as Menu;
use Timber\Post as Post;
use Timber\Timber as Timber;
use Timber\PostQuery;

class IconWebsite{
    protected   $context;
    public      $debug = false,
                $cache = false; // 600

    public function __construct(){
        $this->context = Timber::get_context();
        $this->add_context();
        $this->render_view();
    }

    /**
     * Echoes out $this->context in a semi-readable format
     */
    public function debug(){
        echo '<pre>' . print_r($this->context, true) . '</pre>';
    }

    /**
     * @return bool
     * Checks whether or not a view file exists for the requested page based off of its slug.
     */
    protected function view_file_exists(){
        return file_exists(get_template_directory() . '/views/pages/' . $this->context['post']->slug . '.twig');
    }

    /**
     * @return bool
     * Checks if this is a post-related page (archive, blog page, etc.)
     */
    protected function is_posts(){
        if(is_home() || is_archive()){
            return true;
        } else{
            return false;
        }
    }

    /**
     * Adds Misc. context
     */
    protected function add_context(){
        // AJAX URL
        $this->context['ajax_url'] = admin_url('admin-ajax.php');

        // Add navigational menus
        $this->context['header_menu']  = new Menu('header-nav');

        // Add current post's content
        $this->context['post'] = new Post();

        // Add all site options
        $this->context['site_options'] = get_fields('options');

        // Include posts if necessary
        if($this->is_posts()){
            $this->context['posts'] = new PostQuery();
        }

        // Do things specific to the frontpage.
        if(is_front_page()){
            $this->context['is_frontpage'] = true;
        }
        
        $this->context['is_a_post'] = is_singular('post');
    }

    /**
     * Figures out which view file is needed and renders it.
     */
    public function render_view(){
        if($this->view_file_exists()){
            $file = 'pages/' . $this->context['post']->slug . '.twig';
        } elseif(is_front_page()){
            $file = 'pages/home.twig';
        } elseif($this->is_posts()){
            $file = 'archive.twig';
        } elseif(is_404()){
            $file = '404.twig';
        } else{
            $file = 'single.twig';
        }

        Timber::render($file, $this->context, $this->cache);
    }
}
