<?php
namespace icong;

class customPostType
{

	public  $id,
        $name_singular,
        $name_plural,
        $description = '',
        $taxonomies = array(),
        $hierarchical = false,
        $public = true,
        $menu_position = 5,
        $menu_icon = 'dashicons-thumbs-up',
        $featured_image_name = 'Featured Image',
        $supports = array(),
        $final_args = array(),
        $labels = array(),
        $rewrite = array();

    public function setArg($arg_name = 'name_singular', $arg_value = '')
    {
        $this->$arg_name = $arg_value;
    }

	public function create()
	{
		$this->labels = array(
			'name' => _x($this->name_singular, 'Post Type General Name', 'icong_text_domain'),
			'singular_name' => _x($this->name_singular, 'Post Type Singular Name', 'icong_text_domain'),
			'menu_name' => __($this->name_plural, 'icong_text_domain'),
			'name_admin_bar' => __($this->name_plural, 'icong_text_domain'),
			'archives' => __($this->name_plural . ' Archives', 'icong_text_domain'),
			'attributes' => __($this->name_singular . ' Attributes', 'icong_text_domain'),
			'parent_item_colon' => __($this->name_singular . 'Item:', 'icong_text_domain'),
			'all_items' => __('All ' . $this->name_plural, 'icong_text_domain'),
			'add_new_item' => __('Add New ' . $this->name_singular, 'icong_text_domain'),
			'add_new' => __('Add New', 'icong_text_domain'),
			'new_item' => __('New ' . $this->name_singular, 'icong_text_domain'),
			'edit_item' => __('Edit ' . $this->name_singular, 'icong_text_domain'),
			'update_item' => __('Update ' . $this->name_singular, 'icong_text_domain'),
			'view_item' => __('View ' . $this->name_singular, 'icong_text_domain'),
			'view_items' => __('View ' . $this->name_plural, 'icong_text_domain'),
			'search_items' => __('Search ' . $this->name_plural, 'icong_text_domain'),
			'not_found' => __('Not found', 'icong_text_domain'),
			'not_found_in_trash' => __('Not found in Trash', 'icong_text_domain'),
			'featured_image' => __($this->featured_image_name, 'icong_text_domain'),
			'set_featured_image' => __('Set ' . strtolower($this->featured_image_name), 'icong_text_domain'),
			'remove_featured_image' => __('Remove ' . strtolower($this->featured_image_name), 'icong_text_domain'),
			'use_featured_image' => __('Use as ' . strtolower($this->featured_image_name), 'icong_text_domain'),
			'insert_into_item' => __('Insert into item', 'icong_text_domain'),
			'uploaded_to_this_item' => __('Uploaded to this item', 'icong_text_domain'),
			'items_list' => __($this->name_plural . ' list', 'icong_text_domain'),
			'items_list_navigation' => __($this->name_plural . ' list navigation', 'icong_text_domain'),
			'filter_items_list' => __('Filter ' . $this->name_plural . ' list', 'icong_text_domain'),
		);

		$this->final_args = array(
			'label' => __($this->name_plural, 'icong_text_domain'),
			'description' => __($this->description, 'icong_text_domain'),
			'labels' => $this->labels,
			'supports' => $this->supports,
			'taxonomies' => $this->taxonomies,
			'hierarchical' => $this->hierarchical,
			'public' => $this->public,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => $this->menu_position,
			'menu_icon' => $this->menu_icon,
			'show_in_admin_bar' => true,
			'show_in_nav_menus' => true,
			'can_export' => true,
			'has_archive' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'capability_type' => 'page',
		);

		if(!empty($this->rewrite) || $this->rewrite === false){
			$this->final_args['rewrite'] = $this->rewrite;
		}

		add_action('init', function () {
			register_post_type($this->id, $this->final_args);
		}, 0);
	}
}
