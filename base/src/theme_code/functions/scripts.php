<?php

function ohah_scripts(){
    wp_enqueue_style('default-styles', get_stylesheet_uri());
    wp_enqueue_script('icong_all', get_stylesheet_directory_uri() . '/all.js', array('jquery'), false, true);

    wp_localize_script('icong_ajax_script', 'icongajax', array('ajaxurl' => admin_url('admin-ajax.php')));
    wp_enqueue_script('icong_ajax_script');
}

add_action('wp_enqueue_scripts', 'icong_scripts');
