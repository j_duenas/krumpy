<?php

function icong_register_navigation()
{
    register_nav_menus(
        array(
            'header-nav'  => __('Header Menu'),
        ));
}

add_action('init', 'icong_register_navigation');
