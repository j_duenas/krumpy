<?php

add_filter('acf/settings/path', 'icong_acf_settings_path');
function icong_acf_settings_path($path){
    $path = get_stylesheet_directory() . '/vendor/acf/';
    return $path;
}
add_filter('acf/settings/dir', 'icong_acf_settings_dir');
function icong_acf_settings_dir($dir){
    $dir = get_stylesheet_directory_uri(__FILE__) . '/vendor/acf/';
    return $dir;
}

$current_user = wp_get_current_user();
if($current_user->user_login !== "icongraphics"){
    add_filter('acf/settings/show_admin', '__return_false');
}

include(get_stylesheet_directory() . '/vendor/acf/acf.php');