<?php
require_once(__DIR__ . '/vendor/autoload.php');
$timber = new Timber\Timber();

require 'theme_code/functions/load_acf.php';
require 'theme_code/functions/scripts.php';
require 'theme_code/classes/CPT.php';
require 'theme_code/classes/IconWebsite.php';

require 'theme_code/functions/menus.php';
require 'theme_code/functions/options.php';
require 'theme_code/functions/shortcodes.php';

function get_id_by_slug($page_slug)
{
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}

